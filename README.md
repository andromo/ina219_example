# Ejemplo inicial para INA219 i2c#

### What is this repository for? ###

* Un ejemplo para empezar a usar el sensor usando un cliente i2c
* Se inicializa el sensor y se lee el voltaje

* Esta forma de trabajar no es la correcta, pero para hacer una prueba vale.
* Se debería registrar un driver cliente i2c que se active cuando esté presente en el sistema el dispositivo.

### How do I get set up? ###

* make para compilar el módulo

### Who do I talk to? ###

* andres@uma.es