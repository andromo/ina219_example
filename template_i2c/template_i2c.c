/*  Copyright (C) 2011 PointChips, inc. */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/slab.h>


struct sample_device {
    struct i2c_client *client;
    /* TODO */
};

/* TODO */
static int sample_i2c_probe(struct i2c_client *client,
                            const struct i2c_device_id *id)
{
    struct sample_device *dev;

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
        printk(KERN_ERR "%s: needed i2c functionality is not supported\n", __func__);
        return -ENODEV;
    }
    else
        printk(KERN_INFO "%s: i2c functionality supported\n", __func__);

    dev = kzalloc(sizeof(struct sample_device), GFP_KERNEL);
    if (dev == NULL) {
        printk(KERN_ERR "%s: no memory\n", __func__);
        return -ENOMEM;
    }

    dev->client = client;
    i2c_set_clientdata(client, dev);

    // pdata = client->dev.platform_data;

    /* TODO: do something */

    return 0;
}

static int sample_i2c_remove(struct i2c_client *client)
{
    struct sample_client *dev = i2c_get_clientdata(client);
    printk(KERN_INFO "%s: i2c client driver removed\n", __func__);
    /* TODO: do something */

    kfree(dev);
    return 0;
}

static const struct i2c_device_id sample_i2c_id[] = {
    {"sample_i2c_client", 0},
    { }
};

MODULE_DEVICE_TABLE(i2c, sample_i2c_id);

static struct i2c_driver sample_i2c_driver = {
    .probe    = sample_i2c_probe,
    .remove   = sample_i2c_remove,
    .id_table = sample_i2c_id,
    .driver   = {
        .owner = THIS_MODULE,
        .name = "sample_i2c_client",
    },
};

static int sample_i2c_init_driver(void)
{
    printk(KERN_INFO "%s: i2c client driver added\n", __func__);
    return i2c_add_driver(&sample_i2c_driver);
}

static void __exit sample_i2c_exit_driver(void)
{
    i2c_del_driver(&sample_i2c_driver);
    printk(KERN_INFO "%s: i2c client driver deleted\n", __func__);
}

module_init(sample_i2c_init_driver);
module_exit(sample_i2c_exit_driver);

MODULE_DESCRIPTION("Sample I2C client driver");
MODULE_LICENSE("GPL");
